package com.shteken.endrem.blocks;


import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.OreBlock;
import net.minecraft.world.level.block.state.BlockState;


public class OreBlockBase extends OreBlock {
    public OreBlockBase(Properties properties) {
        super(properties);
    }

    @Override
    public int getExpDrop(BlockState state, net.minecraft.world.level.LevelReader reader, BlockPos pos, int fortune, int silktouch) {
        return silktouch == 0 ? (int) (Math.random() * 8) : 0;
    }
}