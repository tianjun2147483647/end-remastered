package com.shteken.endrem.blocks;

import com.shteken.endrem.properties.FrameProperties;
import com.shteken.endrem.util.RegistryHandler;
import com.google.common.base.Predicates;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.HorizontalDirectionalBlock;
import net.minecraft.world.level.block.Mirror;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.pattern.BlockInWorld;
import net.minecraft.world.level.block.state.pattern.BlockPattern;
import net.minecraft.world.level.block.state.pattern.BlockPatternBuilder;
import net.minecraft.world.level.block.state.predicate.BlockStatePredicate;
import net.minecraft.world.level.block.state.properties.DirectionProperty;
import net.minecraft.world.level.block.state.properties.EnumProperty;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;

public class AncientPortalFrame extends Block {
    public static final EnumProperty<FrameProperties> FRAME_PROPERTY = EnumProperty.create("frame_type", FrameProperties.class);
    public static final DirectionProperty FACING = HorizontalDirectionalBlock.FACING;
    public static final EnumProperty<FrameProperties> EYE = FRAME_PROPERTY;

    protected static final VoxelShape BASE_SHAPE = Block.box(0.0D, 0.0D, 0.0D, 16.0D, 13.0D, 16.0D);
    protected static final VoxelShape EYE_SHAPE = Block.box(4.0D, 13.0D, 4.0D, 12.0D, 16.0D, 12.0D);
    protected static final VoxelShape FULL_SHAPE = Shapes.or(BASE_SHAPE, EYE_SHAPE);
    private static BlockPattern portalShape;

    public static boolean hasEye(BlockState state) {
        return state.getValue(EYE) != FrameProperties.EMPTY;
    }

    public AncientPortalFrame(BlockBehaviour.Properties properties) {
        super(properties);
        this.registerDefaultState(this.stateDefinition.any().setValue(FACING, Direction.NORTH).setValue(EYE, FrameProperties.EMPTY));
    }


    public VoxelShape getShape(BlockState state, BlockGetter blockReader, BlockPos pos, CollisionContext context) {

        return hasEye(state) ? FULL_SHAPE : BASE_SHAPE;
    }

    public BlockState getStateForPlacement(BlockPlaceContext useContext) {
        return this.defaultBlockState().setValue(FACING, useContext.getHorizontalDirection().getOpposite()).setValue(EYE, FrameProperties.EMPTY);
    }

    public boolean hasAnalogOutputSignal(BlockState state) {
        return true;
    }

    public int getAnalogOutputSignal(BlockState state, Level levelIn, BlockPos pos) {
        return hasEye(state) ? 15 : 0;
    }

    public BlockState rotate(BlockState state, Rotation rot) {
        return state.setValue(FACING, rot.rotate(state.getValue(FACING)));
    }

    public BlockState mirror(BlockState state, Mirror mirror) {
        return state.rotate(mirror.getRotation(state.getValue(FACING)));
    }

    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> stateBuilder) {
        stateBuilder.add(FACING, EYE);
    }

    public static BlockPattern getOrCreatePortalShape() {
        if (portalShape == null) {
            portalShape = BlockPatternBuilder.start().aisle("?vvv?", ">???<", ">???<", ">???<", "?^^^?").where('?', BlockInWorld.hasState(BlockStatePredicate.ANY)).where('^', BlockInWorld.hasState(BlockStatePredicate.forBlock(RegistryHandler.ANCIENT_PORTAL_FRAME.get()).where(EYE, Predicates.not(Predicates.equalTo(FrameProperties.EMPTY))).where(FACING, Predicates.equalTo(Direction.SOUTH)))).where('>', BlockInWorld.hasState(BlockStatePredicate.forBlock(RegistryHandler.ANCIENT_PORTAL_FRAME.get()).where(EYE, Predicates.not(Predicates.equalTo(FrameProperties.EMPTY))).where(FACING, Predicates.equalTo(Direction.WEST)))).where('v', BlockInWorld.hasState(BlockStatePredicate.forBlock(RegistryHandler.ANCIENT_PORTAL_FRAME.get()).where(EYE, Predicates.not(Predicates.equalTo(FrameProperties.EMPTY))).where(FACING, Predicates.equalTo(Direction.NORTH)))).where('<', BlockInWorld.hasState(BlockStatePredicate.forBlock(RegistryHandler.ANCIENT_PORTAL_FRAME.get()).where(EYE, Predicates.not(Predicates.equalTo(FrameProperties.EMPTY))).where(FACING, Predicates.equalTo(Direction.EAST)))).build();
        }

        return portalShape;
    }

    /* Check if an eye have already been used to light up the portal */
    public static boolean IsFrameAlreadyUsed(Level levelIn, BlockState frameState, BlockPos pos) {
        for (BlockPos blockPosMutable :
                BlockPos.betweenClosed(pos.offset(4, 0, 4), pos.offset(-4, 0, -4))) {

            if(levelIn.getBlockState(blockPosMutable) != null && levelIn.getBlockState(blockPosMutable).getBlock() == RegistryHandler.ANCIENT_PORTAL_FRAME.get()) {
                if (levelIn.getBlockState(blockPosMutable).getValue(AncientPortalFrame.EYE) == frameState.getValue(AncientPortalFrame.EYE)) {
                    return true;
                }
            }
        }
        return false;
    }
}
