package com.shteken.endrem;

import com.shteken.endrem.config.Config;
import com.shteken.endrem.util.RegistryHandler;
import com.shteken.endrem.util.StructureGenerator;
import com.shteken.endrem.world.gen.OreSpawnHandler;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.Tag;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


@Mod("endrem")
// The value here should match an entry in the META-INF/mods.toml file
public class EndRemastered {
    // Directly reference a log4j logger.
    public static final Logger LOGGER = LogManager.getLogger();
    public static final String MOD_ID = "endrem";
    public static final String NAME = "End Remastered";

    public EndRemastered() {
        // Register ourselves for server and other game events we are interested in
        MinecraftForge.EVENT_BUS.register(this);
        // Load Config
        Config.load();
        RegistryHandler.init();
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
        StructureGenerator.init();
    }

    public static Tag<Block> END_CRYSTAL_GEN;
    private void setup(final FMLCommonSetupEvent event) {
        END_CRYSTAL_GEN = BlockTags.bind("endrem:end_crystal_gen");
        OreSpawnHandler.registerOres();
        event.enqueueWork(StructureGenerator::setup);
    }

    public static final CreativeModeTab TAB = new CreativeModeTab("endremTab") {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(RegistryHandler.ROGUE_EYE.get());
        }
    };
}