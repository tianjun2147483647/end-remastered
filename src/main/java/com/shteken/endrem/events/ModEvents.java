package com.shteken.endrem.events;


import com.shteken.endrem.config.Config;
import net.minecraft.core.BlockPos;
import net.minecraft.world.item.EnderEyeItem;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber
public class ModEvents {

    /* Disable the Eye of Ender to get into a vanilla frame */
    @SubscribeEvent
    public static void DisableUsingEnderEyes(PlayerInteractEvent.RightClickBlock event) {
        BlockPos pos = event.getPos();
        if (event.isCancelable() && !Config.ENABLE_ENDER_EYES.get()) {
            if (event.getPlayer().inventory.getSelected().getItem() instanceof EnderEyeItem) {
                if (event.getWorld().getBlockState(pos).getBlock() == Blocks.END_PORTAL_FRAME) {
                    event.setCanceled(true);
                }
            }
        }
    }

    /* Disable the Eye of Ender to locate the Stronghold */
    @SubscribeEvent
    public static void DisableThrowingEnderEyes(PlayerInteractEvent.RightClickItem event) {
        if (event.isCancelable() && !Config.ENABLE_ENDER_EYES.get()) {
            if (event.getPlayer().inventory.getSelected().getItem() instanceof EnderEyeItem) {
                event.setCanceled(true);
            }
        }
    }
}