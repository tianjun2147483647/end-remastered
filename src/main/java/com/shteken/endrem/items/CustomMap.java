package com.shteken.endrem.items;

import com.shteken.endrem.util.StructureLocator;
import com.shteken.endrem.world.ERStructureConfig.ERStructures;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.npc.VillagerProfession;
import net.minecraft.world.entity.npc.VillagerTrades;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.MapItem;
import net.minecraft.world.item.trading.MerchantOffer;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.levelgen.feature.StructureFeature;
import net.minecraft.world.level.saveddata.maps.MapDecoration;
import net.minecraft.world.level.saveddata.maps.MapItemSavedData;
import net.minecraftforge.event.village.VillagerTradesEvent;
import net.minecraftforge.event.village.WandererTradesEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import javax.annotation.Nonnull;
import java.util.Random;

@Mod.EventBusSubscriber
public class CustomMap {
    private static final int minPrice = 10;
    private static final int maxPrice = 20;
    private static final int experienceGiven = 10;
    private static boolean validStructure = true;

    public static ItemStack createMap(Level level, BlockPos position) {

        if(!(level instanceof ServerLevel))
            return ItemStack.EMPTY;

        ServerLevel serverWorld = (ServerLevel) level;
        StructureLocator structureToLocate = StructureLocator.getStructureToLocateWithMap(serverWorld, position);
        StructureFeature<?> structureFeature = structureToLocate.getStructureFeature();
        BlockPos structurePos = structureToLocate.getLocation();

        validStructure = structurePos != null;
        if(!validStructure) {
            return ItemStack.EMPTY;
        }

        ItemStack stack = MapItem.create(level, structurePos.getX(), structurePos.getZ(), (byte) 2, true, true);
        // fillExplorationMap
        MapItem.renderBiomePreviewMap((ServerLevel) level, stack);
        MapItemSavedData.addTargetDecoration(stack, structurePos, "+", MapDecoration.Type.TARGET_X);
        if (structureFeature == ERStructures.END_GATE.get()){
            stack.setHoverName(new TranslatableComponent("item.endrem.end_gate_map"));
        } else if (structureFeature == ERStructures.END_CASTLE.get()) {
            stack.setHoverName(new TranslatableComponent("item.endrem.end_castle_map"));
        } else {
            stack.setHoverName(Component.nullToEmpty("Endrem Map"));
        }
        return stack;
    }

    @SubscribeEvent
    public static void villagerTrade(VillagerTradesEvent event) {
        if (event.getType() == VillagerProfession.CARTOGRAPHER) {
            if (validStructure) {
                event.getTrades().get(3).add(new CustomMapTrade());
            }
        }
    }

    @SubscribeEvent
    public static void wandererTrades(WandererTradesEvent event) {
        if (validStructure) {
            event.getGenericTrades().add(new CustomMapTrade());
        }
    }

    private static class CustomMapTrade implements VillagerTrades.ItemListing {

        @Override
        public MerchantOffer getOffer(@Nonnull Entity entity, Random random){
            int priceEmeralds = random.nextInt(maxPrice - minPrice + 1) + minPrice;
            ItemStack map = createMap(entity.level, entity.blockPosition());

            if (map != ItemStack.EMPTY) {
                return new MerchantOffer(new ItemStack(Items.EMERALD, priceEmeralds), new ItemStack(Items.COMPASS), map, 12, experienceGiven, 0.2F);
            }
            return null;
        }
    }
}
