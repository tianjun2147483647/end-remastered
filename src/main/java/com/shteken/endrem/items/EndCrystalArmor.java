package com.shteken.endrem.items;

import com.shteken.endrem.EndRemastered;
import com.shteken.endrem.util.RegistryHandler;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.client.renderer.EffectInstance;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.Style;
import net.minecraft.network.chat.TextColor;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.*;
import net.minecraft.world.item.*;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.biome.Biome;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.event.entity.player.CriticalHitEvent;
import net.minecraftforge.eventbus.api.Event;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraft.sounds.SoundEvent;


import javax.annotation.Nullable;
import javax.annotation.ParametersAreNullableByDefault;
import java.util.List;

@Mod.EventBusSubscriber
public class EndCrystalArmor extends ArmorItem {

    public EndCrystalArmor(EquipmentSlot slot) {
        super(new EndCrystalArmorMaterial(), slot, new Item.Properties().tab(EndRemastered.TAB).rarity(Rarity.UNCOMMON));
    }

    @Nullable
    @Override
    public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlot slot, String type) {
        int layer = slot == EquipmentSlot.LEGS ? 2 : 1;

        if ("overlay".equals(type))
            return EndRemastered.MOD_ID + ":textures/models/armor/all_layer_" + layer + "_overlay.png";

        return EndRemastered.MOD_ID + ":textures/models/armor/end_crystal_layer_" + layer + ".png";
    }

    @ParametersAreNullableByDefault
    @OnlyIn(Dist.CLIENT)
    public void appendHoverText(ItemStack stack, Level level, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(new TranslatableComponent("item.endrem.armor.description.main"));
        tooltip.add(Component.nullToEmpty(" "));

        if (Screen.hasShiftDown()) {
            tooltip.add(new TranslatableComponent("item.endrem.armor.description.shift"));
        } else {
            TranslatableComponent shiftMessage = new TranslatableComponent("item.endrem.press_shift");
            shiftMessage.setStyle(Style.EMPTY.withColor(TextColor.parseColor("#5454fc")));
            tooltip.add(shiftMessage);
        }
    }
    @SubscribeEvent
    public static void specialEffects(CriticalHitEvent event) {
        if (event.getResult() == Event.Result.ALLOW || (event.getResult() == Event.Result.DEFAULT && event.isVanillaCritical())) {
            // Duration in ticks * MULTIPLIER
            int duration = 0;
            // Each second is 20 tick
            final int MULTIPLIER = 20;

            for (ItemStack stack : event.getPlayer().getArmorSlots()) {
                if (stack.getItem() instanceof EndCrystalArmor) {
                    duration++;
                }

            }
            duration *= MULTIPLIER;
            if (duration > 0 && event.getTarget().getType().getCategory() == MobCategory.MONSTER) {
                event.getPlayer().addEffect(new MobEffectInstance(MobEffects.REGENERATION, duration, 1));
            }
        }
    }
    @Override
    public boolean makesPiglinsNeutral(ItemStack stack, LivingEntity wearer) {
        return true;
    }

    public static class EndCrystalArmorMaterial implements ArmorMaterial {

        public int getDurabilityForSlot(EquipmentSlot slotIn) {
            return new int[]{13, 15, 16, 11}[slotIn.getIndex()] * 33; // VALUE * MULTIPLIER
        }

        public int getDefenseForSlot(EquipmentSlot slotIn) {
            return new int[]{3, 6, 8, 3}[slotIn.getIndex()];
        }

        public int getEnchantmentValue() {
            return 15;
        }

        public Ingredient getRepairIngredient() {
            return Ingredient.of(RegistryHandler.END_CRYSTAL_INGOT.get());
        }

        public float getToughness() {
            return 2.0F;
        }

        public float getKnockbackResistance() {
            return 0.1F;
        }

        @Override
        public SoundEvent getEquipSound() {
            return SoundEvents.ARMOR_EQUIP_GENERIC;
        }

        @Override
        @OnlyIn(Dist.CLIENT)
        public String getName() {
            return "end_crystal_fragment";
        }
    }
}