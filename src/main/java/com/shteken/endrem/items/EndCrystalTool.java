package com.shteken.endrem.items;

import com.shteken.endrem.EndRemastered;
import com.shteken.endrem.util.RegistryHandler;
import net.minecraft.world.item.*;
import net.minecraft.world.item.crafting.Ingredient;


public class EndCrystalTool{
    public final static Tier MATERIAL = new CustomToolMaterial();

    public static class EndCrystalHoe extends HoeItem {
        public EndCrystalHoe(){
            super(MATERIAL, -4,0.0F, (new Item.Properties()).tab(EndRemastered.TAB));
        }
    }

    public static class EndCrystalPickaxe extends PickaxeItem {
        public EndCrystalPickaxe(){
            super(MATERIAL, 1, -2.8F, (new Item.Properties()).tab(EndRemastered.TAB));
        }
    }

    public static class EndCrystalAxe extends AxeItem {
        public EndCrystalAxe(){
            super(MATERIAL, 5.0F, -3.0F, (new Item.Properties()).tab(EndRemastered.TAB));
        }
    }

    public static class EndCrystalSword extends SwordItem {
        public EndCrystalSword(){
            super(MATERIAL, 3, -2.4F, (new Item.Properties()).tab(EndRemastered.TAB));
        }
    }

    public static class EndCrystalShovel extends ShovelItem {
        public EndCrystalShovel(){
            super(MATERIAL, 1.5F, -3.0F, (new Item.Properties()).tab(EndRemastered.TAB));
        }
    }

    public static class CustomToolMaterial implements Tier {
        public int getUses() {
            return 1000;
        }
        public float getSpeed() {
            return 11.0F;
        }
        public float getAttackDamageBonus() {
            return 5.0F;
        }
        public int getLevel() {
            return 4;
        }
        public int getEnchantmentValue() {
            return 10;
        }
        public Ingredient getRepairIngredient() {
            return Ingredient.of(RegistryHandler.END_CRYSTAL_FRAGMENT.get());
        }
    }
}
