package com.shteken.endrem.util;

import com.google.common.collect.Lists;
import net.minecraft.core.BlockPos;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.levelgen.feature.StructureFeature;
import net.minecraftforge.fml.ModList;
import net.minecraftforge.registries.ForgeRegistries;

import java.util.List;

public class StructureLocator {
    public static List<String> structuresToLocateWithMap = Lists.newArrayList("endrem:end_castle");
    public static List<String> structuresToLocateWithEyes = Lists.newArrayList("endrem:end_gate");

    private BlockPos location;
    private StructureFeature<?> structureFeature;

    private StructureLocator(StructureFeature<?> structureFeatureIn, BlockPos locationIn) {
        this.location = locationIn;
        this.structureFeature = structureFeatureIn;
    }

    public StructureFeature<?> getStructureFeature() {
        return this.structureFeature;
    }

    public BlockPos getLocation() {
        return this.location;
    }

    public static StructureLocator getStructureToLocateWithEyes(ServerLevel serverLevel, BlockPos playerPos) {
        return getStructureToLocate(serverLevel, playerPos, true);
    }

    public static StructureLocator getStructureToLocateWithMap(ServerLevel serverLevel, BlockPos playerPos) {
        return getStructureToLocate(serverLevel, playerPos, false);
    }

    private static StructureLocator getStructureToLocate(ServerLevel serverLevel, BlockPos playerPos, Boolean eyes) {
        // Temporary values
        int shortestDistance = -1;
        BlockPos nearestStructurePos = null;
        StructureFeature<?> nearestStructureFeature = null;

        List<String> structuresToLocate = eyes ? structuresToLocateWithEyes : structuresToLocateWithMap;

        for (String structureID : structuresToLocate) {
            String mod_id = structureID.split(":")[0];

            // Checks if mod is loaded
            if (ModList.get().isLoaded(mod_id)) {
                ResourceLocation resourceLocation = new ResourceLocation(structureID);

                // Checks if structure exists
                if (ForgeRegistries.STRUCTURE_FEATURES.containsKey(resourceLocation)) {
                    // Get distance from player and add it to the list
                    StructureFeature<?> structureFeature = ForgeRegistries.STRUCTURE_FEATURES.getValue(resourceLocation);
                    BlockPos structurePos = serverLevel.getChunkSource().getGenerator().findNearestMapFeature(serverLevel, structureFeature, playerPos, 100, false);

                    if (structurePos != null) {
                        int distance = getDistance(structurePos.getX(), playerPos.getX(), structurePos.getZ(), playerPos.getZ());
                        if ((shortestDistance > distance || shortestDistance == -1)) {
                            shortestDistance = distance;
                            nearestStructurePos = structurePos;
                            nearestStructureFeature = structureFeature;
                        }
                    }
                }
            }
        }

        return new StructureLocator(nearestStructureFeature, nearestStructurePos);
    }

    private static int getDistance(int x1, int x2, int z1, int z2) {
        return (int) Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((z2 - z1), 2));
    }
}
