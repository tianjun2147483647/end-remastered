package com.shteken.endrem.util;

import com.shteken.endrem.EndRemastered;
import com.shteken.endrem.blocks.*;
import com.shteken.endrem.items.*;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.RecordItem;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.levelgen.feature.StructureFeature;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.level.material.MaterialColor;
import net.minecraftforge.common.loot.GlobalLootModifierSerializer;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.fmllegacy.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class RegistryHandler {

    /* Register */
    public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, EndRemastered.MOD_ID);
    public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, EndRemastered.MOD_ID);
    public static final DeferredRegister<SoundEvent> SOUNDS = DeferredRegister.create(ForgeRegistries.SOUND_EVENTS, EndRemastered.MOD_ID);
    public static final DeferredRegister<GlobalLootModifierSerializer<?>> GLMS = DeferredRegister.create(ForgeRegistries.LOOT_MODIFIER_SERIALIZERS, EndRemastered.MOD_ID);

    public static void init() {
        IEventBus modEventBus = FMLJavaModLoadingContext.get().getModEventBus();

        ITEMS.register(modEventBus);
        BLOCKS.register(modEventBus);
        SOUNDS.register(modEventBus);
        GLMS.register(modEventBus);
    }

    /* Entities Eyes */
    public static final RegistryObject<Item> MAGICAL_EYE = ITEMS.register("magical_eye", CustomEye::new);
    public static final RegistryObject<Item> WITHER_EYE = ITEMS.register("wither_eye", CustomEye::new);
    public static final RegistryObject<Item> GUARDIAN_EYE = ITEMS.register("guardian_eye", CustomEye::new);

    /* Chest Eyes */
    public static final RegistryObject<Item> OLD_EYE = ITEMS.register("old_eye", CustomEye::new);
    public static final RegistryObject<Item> ROGUE_EYE = ITEMS.register("rogue_eye", CustomEye::new);
    public static final RegistryObject<Item> NETHER_EYE = ITEMS.register("nether_eye", CustomEye::new);
    public static final RegistryObject<Item> COLD_EYE = ITEMS.register("cold_eye", CustomEye::new);
    public static final RegistryObject<Item> CORRUPTED_EYE = ITEMS.register("corrupted_eye", CustomEye::new);
    public static final RegistryObject<Item> BLACK_EYE = ITEMS.register("black_eye", CustomEye::new);
    public static final RegistryObject<Item> LOST_EYE = ITEMS.register("lost_eye", CustomEye::new);

    /* Craftable Eyes */
    public static final RegistryObject<Item> END_CRYSTAL_EYE = ITEMS.register("end_crystal_eye", CustomEye::new);
    public static final RegistryObject<Item> WITCH_EYE = ITEMS.register("witch_eye", CustomEye::new);

    /* Miscellaneous */
    public static final RegistryObject<Item> WITCH_PUPIL = ITEMS.register("witch_pupil", () -> new Item(new Item.Properties().tab(EndRemastered.TAB)));
    public static final RegistryObject<Item> END_CRYSTAL_FRAGMENT = ITEMS.register("end_crystal_fragment", () -> new Item(new Item.Properties().fireResistant().tab(EndRemastered.TAB)));
    public static final RegistryObject<Item> END_CRYSTAL_INGOT = ITEMS.register("end_crystal_ingot", () -> new Item(new Item.Properties().tab(EndRemastered.TAB)));

    /* Interactive  Block */
    public static final RegistryObject<Block> ANCIENT_PORTAL_FRAME = BLOCKS.register("ancient_portal_frame", () -> new AncientPortalFrame(BlockBehaviour.Properties.of(Material.STONE, MaterialColor.COLOR_GRAY).sound(SoundType.GLASS).lightLevel((p_152690_) -> {
        return 1;
    }).strength(-1.0F, 3600000.0F).noDrops()));

    /* Blocks */
    public static final RegistryObject<Block> END_CRYSTAL_ORE = BLOCKS.register("end_crystal_ore", () -> new OreBlockBase(Block.Properties.copy(Blocks.ANCIENT_DEBRIS)));
    public static final RegistryObject<Block> END_CRYSTAL_BLOCK = BLOCKS.register("end_crystal_block", EndCrystalBlock::new);
    /* Block Items */
    public static final RegistryObject<Item> ANCIENT_PORTAL_FRAME_ITEM = ITEMS.register("ancient_portal_frame", () -> new BlockItem(ANCIENT_PORTAL_FRAME.get(), new Item.Properties().tab(EndRemastered.TAB)));
    public static final RegistryObject<Item> END_CRYSTAL_ORE_ITEM = ITEMS.register("end_crystal_ore", () -> new BlockItem(END_CRYSTAL_ORE.get(), new Item.Properties().tab(EndRemastered.TAB)));
    public static final RegistryObject<Item> END_CRYSTAL_BLOCK_ITEM = ITEMS.register("end_crystal_block", () -> new BlockItem(END_CRYSTAL_BLOCK.get(), new Item.Properties().tab(EndRemastered.TAB)));

    /* Sounds & Music */
    public static final RegistryObject<SoundEvent> PORTAL_OPEN_SOUND = SOUNDS.register("music.portal_open_sound", () -> new SoundEvent(new ResourceLocation(EndRemastered.MOD_ID, "music.portal_open_sound")));
    public static final RegistryObject<SoundEvent> END_CASTLE_MUSIC = SOUNDS.register("music.end_castle_disc", () -> new SoundEvent(new ResourceLocation(EndRemastered.MOD_ID, "music.end_castle_disc")));
    public static final RegistryObject<Item> END_CASTLE_DISC_ITEM = ITEMS.register("end_castle_disc", () -> new RecordItem(0, END_CASTLE_MUSIC, new Item.Properties().stacksTo(1).tab(EndRemastered.TAB)));

    /* Armors */
    public static final RegistryObject<Item> END_CRYSTAL_HELMET = ITEMS.register("end_crystal_helmet", () -> new EndCrystalArmor(EquipmentSlot.HEAD));
    public static final RegistryObject<Item> END_CRYSTAL_CHESTPLATE = ITEMS.register("end_crystal_chestplate", () -> new EndCrystalArmor(EquipmentSlot.CHEST));
    public static final RegistryObject<Item> END_CRYSTAL_LEGGINGS = ITEMS.register("end_crystal_leggings", () -> new EndCrystalArmor(EquipmentSlot.LEGS));
    public static final RegistryObject<Item> END_CRYSTAL_BOOTS = ITEMS.register("end_crystal_boots", () -> new EndCrystalArmor(EquipmentSlot.FEET));

    /* Tools */
    public static final RegistryObject<Item> END_CRYSTAL_HOE = ITEMS.register("end_crystal_hoe", EndCrystalTool.EndCrystalHoe::new);
    public static final RegistryObject<Item> END_CRYSTAL_PICKAXE = ITEMS.register("end_crystal_pickaxe", EndCrystalTool.EndCrystalPickaxe::new);
    public static final RegistryObject<Item> END_CRYSTAL_AXE = ITEMS.register("end_crystal_axe", EndCrystalTool.EndCrystalAxe::new);
    public static final RegistryObject<Item> END_CRYSTAL_SWORD = ITEMS.register("end_crystal_sword", EndCrystalTool.EndCrystalSword::new);
    public static final RegistryObject<Item> END_CRYSTAL_SHOVEL = ITEMS.register("end_crystal_shovel", EndCrystalTool.EndCrystalShovel::new);

    /* Global Loot Modifiers */
    public static RegistryObject<GlobalLootModifierSerializer<LootInjection.LootInjectionModifier>> LOOT_INJECTION = GLMS.register("loot_injection", LootInjection.Serializer::new);

}
