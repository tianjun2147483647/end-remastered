package com.shteken.endrem.config;

import com.google.common.collect.Lists;
import com.shteken.endrem.EndRemastered;
import com.electronwill.nightconfig.core.file.CommentedFileConfig;
import com.electronwill.nightconfig.core.io.WritingMode;
import com.shteken.endrem.util.StructureGenerator;
import com.shteken.endrem.util.StructureLocator;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.config.ModConfigEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.fml.loading.FMLPaths;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class Config {
    private static final ForgeConfigSpec.Builder CONFIG = new ForgeConfigSpec.Builder();
    private static ForgeConfigSpec COMMON_CONFIG;

    public static ForgeConfigSpec.ConfigValue<Boolean> ENABLE_STRONGHOLDS;
    public static ForgeConfigSpec.ConfigValue<Boolean> ENABLE_ENDER_EYES;
    public static ForgeConfigSpec.ConfigValue<Boolean> END_CASTLE_ENABLED;
    public static ForgeConfigSpec.ConfigValue<Boolean> END_GATE_ENABLED;
    public static ForgeConfigSpec.ConfigValue<Integer> END_CASTLE_DISTANCE;
    public static ForgeConfigSpec.ConfigValue<Integer> END_GATE_DISTANCE;
    public static ForgeConfigSpec.ConfigValue<Integer> ANCIENT_WITCH_HUT_DISTANCE;
    public static ForgeConfigSpec.ConfigValue<Integer> END_GATE_SIZE;
    public static ForgeConfigSpec.ConfigValue<Integer> STRUCTURE_MONSTER_DIFFICULTY;
    public static ForgeConfigSpec.ConfigValue<String> EYES_LOCATE_STRUCTURE;
    public static ForgeConfigSpec.ConfigValue<String> MAP_LOCATES_STRUCTURE;
    public static ForgeConfigSpec.ConfigValue<String> WHITELISTED_DIMENSIONS;

    static {
        initConfig();
        ModLoadingContext.get().registerConfig(net.minecraftforge.fml.config.ModConfig.Type.COMMON, COMMON_CONFIG, "endrem.toml");
        MinecraftForge.EVENT_BUS.addListener(Config::onWorldLoad);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(Config::onUpdateConfig);
    }

    private static void onWorldLoad(WorldEvent.Load event) {
        load();
    }

    private static void onUpdateConfig(ModConfigEvent event) {
        ModConfig config = event.getConfig();

        if (config.getSpec() == COMMON_CONFIG) {
            StructureGenerator.whitelistedDimensions = createListFromConfig(
                    "whitelisted_dimensions",
                    WHITELISTED_DIMENSIONS,
                    Lists.newArrayList("minecraft:overworld"));

            StructureLocator.structuresToLocateWithMap = createListFromConfig(
                    "map_behavior",
                    MAP_LOCATES_STRUCTURE,
                    Lists.newArrayList("endrem:end_castle")
            );

            StructureLocator.structuresToLocateWithEyes = createListFromConfig(
                    "eye_behavior",
                    EYES_LOCATE_STRUCTURE,
                    Lists.newArrayList("endrem:end_gate")
            );
        }
    }

    private static ArrayList<String> createListFromConfig(String settingName, ForgeConfigSpec.ConfigValue<String> configValue, ArrayList<String> defaultValue) {
        String rawString = configValue.get();
        int strLen = rawString.length();

        if (strLen < 2 || rawString.charAt(0) != '[' || rawString.charAt(strLen - 1) != ']') {
            // Create a human-readable version of the default value
            StringBuilder formattedDefaultValue = new StringBuilder("[");

            for (int i = 0; i < defaultValue.size(); i++) {
                formattedDefaultValue.append(defaultValue.get(i));
                if (i != defaultValue.size() - 1){
                    formattedDefaultValue.append(", ");
                }
            }

            // Print warning
            EndRemastered.LOGGER.error(String.format("INVALID VALUE FOR SETTING '%s'. Using %s instead...", settingName, formattedDefaultValue));
            return defaultValue;
        }
        return Lists.newArrayList(rawString.substring(1, strLen - 1).split(",\\s*"));
    }

    private static void initConfig() {
        CONFIG.push(EndRemastered.MOD_ID);

        ENABLE_STRONGHOLDS = CONFIG.comment("Enable/Disable generation of vanilla strongholds")
                .worldRestart().
                define("enable_strongholds", false);
        ENABLE_ENDER_EYES = CONFIG.comment("Enable/Disable usage of Ender Eyes")
                .define("enable_ender_eyes", false);

        END_CASTLE_ENABLED = CONFIG.comment("Enable/Disable the End Castle")
                .worldRestart()
                .define("enable_end_castle", true);
        END_CASTLE_DISTANCE = CONFIG.comment("Distance in chunks between End Castles")
                .worldRestart()
                .define("castle_distance", 250);

        END_GATE_ENABLED = CONFIG.comment("Enable/Disable the End Gate")
                .worldRestart()
                .define("enable_end_gate", true);
        END_GATE_DISTANCE = CONFIG.comment("Distance in chunks between End Gates")
                .worldRestart()
                .define("end_gate_distance", 250);
        END_GATE_SIZE = CONFIG.comment("Size of the End Gates")
                .worldRestart()
                .define("end_gate_size", 20);

        ANCIENT_WITCH_HUT_DISTANCE = CONFIG.comment("Distance in chunks between ancient witch huts")
                .worldRestart()
                .define("witch_hut_distance", 25);

        STRUCTURE_MONSTER_DIFFICULTY = CONFIG.comment("Sets number of mobs inside the End Gate and the End Castle --> 0: No Mobs, 1: Easy, 2: Normal, 3: Hard, 4: Nightmare")
                .define("structure_mob_difficulty", 2);

        EYES_LOCATE_STRUCTURE = CONFIG.comment("Changes the structure that End Remastered eyes track (set value to \"[null]\" to disable)")
                .define("eye_behavior", "[endrem:end_gate]");

        MAP_LOCATES_STRUCTURE = CONFIG.comment("Changes the structure that the End Remastered map locates (set value to \"[null]\" to disable)")
                .define("map_behavior", "[endrem:end_castle]");

        WHITELISTED_DIMENSIONS = CONFIG.comment("List of whitelisted dimensions for structure generation")
                .worldRestart()
                .define("whitelisted_dimensions", "[minecraft:overworld]");

        CONFIG.pop();
        COMMON_CONFIG = CONFIG.build();
    }

    public static double getDifficultyMultiplier() {
        switch (STRUCTURE_MONSTER_DIFFICULTY.get()) {
            case 0:
                return 0;
            case 1:
                return 0.5;
            case 2:
            default:
                return 1;
            case 3:
                return 1.5;
            case 4:
                return 2.25;
        }
    }

    public static TranslatableComponent getDifficultyName() {
        switch (STRUCTURE_MONSTER_DIFFICULTY.get()) {
            case 0:
                return new TranslatableComponent("endrem.config.structure_difficulty.no_mobs");
            case 1:
                return new TranslatableComponent("endrem.config.structure_difficulty.easy");
            case 2:
            default:
                return new TranslatableComponent("endrem.config.structure_difficulty.normal");
            case 3:
                return new TranslatableComponent("endrem.config.structure_difficulty.hard");
            case 4:
                return new TranslatableComponent("endrem.config.structure_difficulty.nightmare");
        }
    }

    public static void load() {
        final CommentedFileConfig configData = CommentedFileConfig.builder(FMLPaths.CONFIGDIR.get().resolve("endrem.toml"))
                .sync()
                .autosave()
                .writingMode(WritingMode.REPLACE)
                .build();

        configData.load();
        COMMON_CONFIG.setConfig(configData);
    }
}
