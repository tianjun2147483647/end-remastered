package com.shteken.endrem.world.gen;

import com.shteken.endrem.EndRemastered;
import com.shteken.endrem.util.RegistryHandler;
import net.minecraft.core.Registry;
import net.minecraft.data.BuiltinRegistries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.world.level.levelgen.VerticalAnchor;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.configurations.FeatureConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.OreConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.RangeDecoratorConfiguration;
import net.minecraft.world.level.levelgen.heightproviders.UniformHeight;
import net.minecraft.world.level.levelgen.structure.templatesystem.TagMatchTest;
import net.minecraftforge.common.world.BiomeGenerationSettingsBuilder;
import net.minecraftforge.event.world.BiomeLoadingEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;


@Mod.EventBusSubscriber
public class OreSpawnHandler {

    public static ConfiguredFeature<?, ?> END_CRYSTAL_ORE_GEN;

    public static void registerOres() {

        END_CRYSTAL_ORE_GEN = register("end_crystal_ore", Feature.ORE
                .configured(new OreConfiguration(new TagMatchTest(EndRemastered.END_CRYSTAL_GEN),
                        RegistryHandler.END_CRYSTAL_ORE.get().defaultBlockState(),
                        3, // Vein size
                        0.5F))  // Exposition of the Ore
                .range(new RangeDecoratorConfiguration(UniformHeight.of(VerticalAnchor.bottom(), VerticalAnchor.absolute(150)))) // ???
                .squared()
                .count(30));
    }

    @SubscribeEvent(priority = EventPriority.HIGHEST)
    public static void gen(BiomeLoadingEvent event) {
        BiomeGenerationSettingsBuilder generation = event.getGeneration();
        if (event.getCategory().equals(Biome.BiomeCategory.NETHER)) {
            if (END_CRYSTAL_ORE_GEN != null)
                generation.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, END_CRYSTAL_ORE_GEN);
        }
    }

    private static <FC extends FeatureConfiguration> ConfiguredFeature<FC, ?> register(String name, ConfiguredFeature<FC, ?> configureFeature) {
        return Registry.register(BuiltinRegistries.CONFIGURED_FEATURE, new ResourceLocation(EndRemastered.MOD_ID + ":" + name), configureFeature);
    }

}

