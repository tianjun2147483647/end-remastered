package com.shteken.endrem.properties;

import com.shteken.endrem.util.RegistryHandler;
import net.minecraft.util.StringRepresentable;
import net.minecraft.world.item.Item;

public enum FrameProperties implements StringRepresentable {
    EMPTY,
    OLD_EYE,
    ROGUE_EYE,
    NETHER_EYE,
    COLD_EYE,
    CORRUPTED_EYE,
    MAGICAL_EYE,
    BLACK_EYE,
    LOST_EYE,
    WITHER_EYE,
    END_CRYSTAL_EYE,
    GUARDIAN_EYE,
    WITCH_EYE;

    public String toString() {
        return this.getSerializedName();
    }

    public String getSerializedName() {
        switch (this) {
            case EMPTY: default:
                return "empty";
            case OLD_EYE:
                return "old_eye";
            case ROGUE_EYE:
                return "rogue_eye";
            case NETHER_EYE:
                return "nether_eye";
            case COLD_EYE:
                return "cold_eye";
            case CORRUPTED_EYE:
                return "corrupted_eye";
            case MAGICAL_EYE:
                return "magical_eye";
            case BLACK_EYE:
                return "black_eye";
            case LOST_EYE:
                return "lost_eye";
            case WITHER_EYE:
                return "wither_eye";
            case END_CRYSTAL_EYE:
                return "end_crystal_eye";
            case GUARDIAN_EYE:
                return "guardian_eye";
            case WITCH_EYE:
                return "witch_eye";
        }
    }

    public static FrameProperties getFrameProperty(Item eye) {
        FrameProperties frameProperties = FrameProperties.EMPTY;

        if (eye == RegistryHandler.OLD_EYE.get()) {
            frameProperties = FrameProperties.OLD_EYE;
        } else if (eye == RegistryHandler.ROGUE_EYE.get()) {
            frameProperties = FrameProperties.ROGUE_EYE;
        } else if (eye == RegistryHandler.NETHER_EYE.get()) {
            frameProperties = FrameProperties.NETHER_EYE;
        } else if (eye == RegistryHandler.COLD_EYE.get()) {
            frameProperties = FrameProperties.COLD_EYE;
        } else if (eye == RegistryHandler.CORRUPTED_EYE.get()) {
            frameProperties = FrameProperties.CORRUPTED_EYE;
        } else if (eye == RegistryHandler.MAGICAL_EYE.get()) {
            frameProperties = FrameProperties.MAGICAL_EYE;
        } else if (eye == RegistryHandler.BLACK_EYE.get()) {
            frameProperties = FrameProperties.BLACK_EYE;
        } else if (eye == RegistryHandler.LOST_EYE.get()) {
            frameProperties = FrameProperties.LOST_EYE;
        } else if (eye == RegistryHandler.WITHER_EYE.get()) {
            frameProperties = FrameProperties.WITHER_EYE;
        } else if (eye == RegistryHandler.END_CRYSTAL_EYE.get()) {
            frameProperties = FrameProperties.END_CRYSTAL_EYE;
        } else if (eye == RegistryHandler.GUARDIAN_EYE.get()) {
            frameProperties = FrameProperties.GUARDIAN_EYE;
        } else if (eye == RegistryHandler.WITCH_EYE.get()) {
            frameProperties = FrameProperties.WITCH_EYE;
        }

        return frameProperties;
    }
}
